angular.module('home').service('ProjectService', function ($http) {
  var service = {
    getAllProjects: function () {
      return $http.get('data/projects.json', { cache: true }).then(function (resp) {
        return resp.data;
      });
    },

    getProject: function (name) {
      function projectMatchesParam (project) {
        return project.name === name;
      }

      return service.getAllProjects().then(function (projects) {
        return projects.find(projectMatchesParam)
      });
    }
  }

  return service;
})
