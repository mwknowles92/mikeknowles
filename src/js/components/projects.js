angular.module('home').component('projects', {
  bindings: { projects: '<' },
  templateUrl: 'projects.html'
});
