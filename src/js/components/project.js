angular.module('home').component('project', {
  bindings: { project: '<' },
  templateUrl: 'project.html'
});
