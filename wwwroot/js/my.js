var homeApp = angular.module('home', ['ui.router', 'ngSanitize']);

// Rewrite URLs so that spaces become '-' instead of '%20'
// Make them still route correctly
var projectType = {
  encode: function (str) { return str && str.replace(/ /g, "-"); },
  decode: function (str) { return str && str.replace(/-/g, " "); },
  is: angular.isString,
  pattern: /[^/]+/
};
homeApp.config(function ($stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider) {
  $urlMatcherFactoryProvider.type('project', projectType);
});

homeApp.config(function ($stateProvider) {
    var states = [
        {
            name: 'projects',
            url: '/',
            component: 'projects',
            resolve: {
                projects: function (ProjectService) {
                    return ProjectService.getAllProjects();
                }
            }
        },

        {
            name: 'project',
            url: '^/{projectName:project}',
            component: 'project',
            resolve: {
                project: function (ProjectService, $transition$) {
                  return ProjectService.getProject($transition$.params().projectName);
                }
            }
        }
    ]

    // Register all states
    states.forEach(function (state) {
      $stateProvider.state(state);
    });
});

// Auto route to '/' if no other url works or is given
homeApp.config(['$urlRouterProvider',
  function ($urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
  }
]);

// Remove the # from urls
homeApp.config(["$locationProvider",
  function ($locationProvider) {
    $locationProvider.html5Mode(true);
  }
]);

homeApp.run(function ($http) {
    $http.get('data/projects.json', { cache: true });
});

angular.module('home').service('ProjectService', function ($http) {
  var service = {
    getAllProjects: function () {
      return $http.get('data/projects.json', { cache: true }).then(function (resp) {
        return resp.data;
      });
    },

    getProject: function (name) {
      function projectMatchesParam (project) {
        return project.name === name;
      }

      return service.getAllProjects().then(function (projects) {
        return projects.find(projectMatchesParam)
      });
    }
  }

  return service;
})

angular.module('home').component('project', {
  bindings: { project: '<' },
  templateUrl: 'project.html'
});

angular.module('home').component('projects', {
  bindings: { projects: '<' },
  templateUrl: 'projects.html'
});
