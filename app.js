var compression = require('compression');
var express = require('express');
var path = require('path');

var app = express();

var root = 'wwwroot';

app.use(compression());
app.use(express.static(root))

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, root, 'index.html'))
})

// For html5Mode
app.all('/*', function (req, res) {
  res.sendFile(path.join(__dirname, root, 'index.html'))
});

app.listen(3000);
