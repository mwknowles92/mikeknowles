var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var del = require('del');
var pump = require('pump');
var concat = require('gulp-concat');
var server = require('gulp-express');
var Hjson = require('gulp-hjson');

var pub_www = 'wwwroot/';
var pub_json = pub_www + 'data/';
var pub_js = pub_www + 'js/';
var pub_css = pub_www + 'css/';
var pub_image = pub_www + 'images/';

var src = 'src/';
var src_json = src + 'data/';
var src_js = src + 'js/';
var src_css = src + 'css/';
var src_image = src + 'images/';

gulp.task('clean', function () {
  return del([
    pub_js + '**/*',
    pub_css + '**/*',
    pub_image + '**/*'
  ]);
});

gulp.task('hjson', function (cb) {
  pump([
    gulp.src([
      src_json + '*.hjson'
    ]),
    Hjson({ to: 'json' }),
    gulp.dest(pub_json)
  ], cb);
});

gulp.task('css', function (cb) {
  pump([
    gulp.src([
      src_css + '*.scss'
    ]),
    sass().on('error', sass.logError),
    concat('styles.css'),
    cleanCSS(),
    gulp.dest(pub_css)
  ], cb);
});

gulp.task('vendor-js', function (cb) {
  pump([
    gulp.src([
      'node_modules/angular/angular.js',
      // 'node_modules/angular-animate/angular-animate.js',
      // 'node_modules/angular-aria/angular-aria.js',
      // 'node_modules/angular-material/angular-material.js',
      'node_modules/angular-ui-router/release/angular-ui-router.js',
      'node_modules/angular-sanitize/angular-sanitize.js'
      // 'node_modules/jquery/dist/jquery.js',
      // 'node_modules/jquery-validation/dist/jquery.validate.js',
      // 'node_modules/jquery-validation-unobtrusive/jquery.validate.unobtrusive.js'
    ]),
    uglify({mangle: false}),
    concat('vendor.js'),
    gulp.dest(pub_js)
  ], cb);
});

gulp.task('my-js', function (cb) {
  pump([
    gulp.src([
      src_js + 'homeApp.js',
      src_js + 'services/*.js',
      src_js + 'components/*.js'
    ]),
    // uglify({mangle: false}),
    concat('my.js'),
    gulp.dest(pub_js)
  ], cb);
});

gulp.task('image', function (cb) {
  pump([
    gulp.src(src_image + '*'),
    imagemin(),
    gulp.dest(pub_image)
  ], cb);
});

gulp.task('server', function (cb) {
  server.run(['app.js']);

  gulp.watch(src_json + '*.hjson', function (event) {
    gulp.run('hjson');
    server.notify(event);
  });
  gulp.watch(src_css + '*.scss', function (event) {
    gulp.run('css');
    server.notify(event);
  });
  gulp.watch(src_js + '**/*.js', function (event) {
    gulp.run('my-js');
    server.notify(event);
  });
  gulp.watch(src_image + '*', function (event) {
    gulp.run('image');
    server.notify(event);
  });
});

gulp.task('build', ['hjson', 'css', 'vendor-js', 'my-js', 'image']);

gulp.task('default', ['build', 'server']);
